from sqlalchemy.sql.sqltypes import Date
from sqlalchemy import Column, String, Integer
from sqlalchemy_utils import UUIDType

from Domain.Entities.Base import Base

class News(Base) : 

    __tablename__ = 'site_news'

    Id =  Column('id', UUIDType(binary=True), primary_key=True)
    Title = Column('title', String)
    Content = Column('content', String)
    CreatedAt = Column('created_at', Date, primary_key=True)
    IsDeleted = Column('is_deleted', Integer)
