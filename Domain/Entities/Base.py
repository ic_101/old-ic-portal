from sqlalchemy.orm import declarative_base
from sqlalchemy.sql.schema import Column, ForeignKey, Table

Base = declarative_base()

ReportGroupManyToMany = Table('site_report_group', Base.metadata,
    Column('report_id', ForeignKey('site_reports.id')),
    Column('group_id', ForeignKey('site_report_groups.id'))
)

ReportCategoryManyToMany = Table('site_report_category_group', Base.metadata,
    Column('category_id', ForeignKey('site_report_categories.id')),
    Column('group_id', ForeignKey('site_report_groups.id'))
)