from sqlalchemy import CLOB, Column, String, Integer
from sqlalchemy_utils import UUIDType

from Domain.Entities.Base import Base

class Report(Base) : 

    __tablename__ = 'site_reports'

    Id =  Column('id', UUIDType(binary=True), primary_key=True)
    Title = Column('title', String)
    Query = Column('query', CLOB)
    IsDeleted = Column('is_deleted', Integer)
    ChartsType = Column('charts_type', Integer)
    DataType = Column('data_type', Integer)
    Columns = Column('columns', String)
    
