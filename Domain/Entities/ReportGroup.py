from typing import List

from sqlalchemy import Column, String
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm import relationship

from Domain.Entities.Base import Base, ReportGroupManyToMany
from Domain.Entities.Report import Report

class ReportGroup(Base) : 

    __tablename__ = 'site_report_groups'

    Id =  Column('id', UUIDType(binary=True), primary_key=True)
    Title = Column('title', String)

    Reports:List[Report] = relationship('Report', secondary=ReportGroupManyToMany, lazy="joined")