from typing import List

from sqlalchemy import Column, String
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm import relationship

from Domain.Entities.Base import Base, ReportCategoryManyToMany
from Domain.Entities.ReportGroup import ReportGroup

class ReportCategory(Base) : 

    __tablename__ = 'site_report_categories'

    Id =  Column('id', UUIDType(binary=True), primary_key=True)
    Title = Column('title', String)

    Groups:List[ReportGroup] = relationship('ReportGroup', secondary=ReportCategoryManyToMany, lazy="joined")