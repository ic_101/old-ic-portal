from sqlalchemy.orm.exc import NoResultFound
from Application.Services.ConfigurationService import ConfigurationService
from fastapi.applications import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from Application.Handlers.NoResultFoundHandler import NoResultFoundHandler
from Application.Handlers.ExceptionHandler import ExceptionHandler

class FastApiService : 

    _fastApi:FastAPI
    _configurationService:ConfigurationService

    def __init__(
        self, 
        configurationService:ConfigurationService) :
        self._configurationService = configurationService
        self.CreateFastAPI()
        self.ActivateStaticFiles()
        self.AddExceptionHandlers()
        self.EnableCORS()

    def CreateFastAPI(self) : 
        self._fastApi = FastAPI(
                            title=self._configurationService.FastApiTitle, 
                            version=self._configurationService.FastApiVersion, 
                            description=self._configurationService.FastApiDescription, 
                            docs_url=None, 
                            redoc_url=None)
        
    def ActivateStaticFiles(self) :
        self._fastApi.mount("/static/api-documentation", StaticFiles(directory="Static/ApiDocumentation"), name="/static/api-documentation")

    def AddExceptionHandlers(self) : 
        self._fastApi.add_exception_handler(NoResultFound, NoResultFoundHandler().OnException)
        self._fastApi.add_exception_handler(Exception, ExceptionHandler().OnException)

    def EnableCORS(self) : 
        self._fastApi.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"])

    @property
    def FastApiWithCORS(self) : 
        return CORSMiddleware(
        self._fastApi,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    