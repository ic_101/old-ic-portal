from Application.Controllers.ReportCategoriesController import ReportCategoriesController
from Application.Services.ConfigurationService import ConfigurationService
from Infrastructure.Services.SessionService import SessionService
from Application.Controllers.DocumentationController import DocumentationController
from Application.Controllers.NewsController import NewsController
from Application.Controllers.ReportsController import ReportsController
from Startup.FastApiService import FastApiService
from Startup.UvicornService import UvicornService

class DependenciesService() :

    _configurationService:ConfigurationService
    _fastApiService:FastApiService
    _uvicornService:UvicornService
    _sessionService:SessionService

    def __init__(self) -> None:
        self._configurationService = None
        self._sessionService = None
        self._fastApiService = None
        self._uvicornService = None

    @property
    def ConfigurationService(self) : 
        if not self._configurationService :
            self._configurationService = ConfigurationService()
        return self._configurationService

    @property
    def FastApiService(self) -> FastApiService : 
        if not self._fastApiService :
            self._fastApiService = FastApiService(
                self.ConfigurationService,
            )
        return self._fastApiService

    @property
    def UvicornService(self) : 
        if not self._uvicornService :
            self._uvicornService = UvicornService(
                self.ConfigurationService,
                self.FastApiService,
            )
        return self._uvicornService

    @property
    def SessionService(self) : 
        if not self._sessionService :
            self._sessionService = SessionService(
                self.ConfigurationService
            )
        return self._sessionService

    def RegisterControllers(self) :
        DocumentationController(
            self.FastApiService
        )

        NewsController(
            self.FastApiService,
            self.SessionService
        )

        ReportsController(
            self.FastApiService,
            self.SessionService
        )

        ReportCategoriesController(
            self.FastApiService,
            self.SessionService
        )