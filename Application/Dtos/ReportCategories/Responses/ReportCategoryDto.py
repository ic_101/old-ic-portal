from typing import List

from Application.Dtos.ReportGroups.Reponses.ReportGroupDto import ReportGroupDto
from Application.Dtos.Reports.Responses.ReportDto import ReportDto
from Domain.Entities.ReportCategory import ReportCategory

class ReportCategoryDto : 
    Id:str
    Title:str
    Groups:List[ReportGroupDto]

    def __init__(self, model:ReportCategory) -> None:
        self.Id = model.Id.hex
        self.Title = model.Title

        reportGroupDtos = []

        for reportGroupModel in model.Groups :
            reportGroupDtos.append(ReportGroupDto(reportGroupModel))

        self.Groups = reportGroupDtos