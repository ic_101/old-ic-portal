from typing import List

from Application.Dtos.Reports.Responses.ReportDto import ReportDto
from Domain.Entities.ReportGroup import ReportGroup

class ReportGroupDto : 
    Id:str
    Title:str
    Reports:List[ReportDto]

    def __init__(self, model:ReportGroup) -> None:
        self.Id = model.Id.hex
        self.Title = model.Title

        reportDtos = []

        for reportModel in model.Reports :
            reportDtos.append(ReportDto(reportModel))

        self.Reports = reportDtos