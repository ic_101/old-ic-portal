from Domain.Entities.Report import Report

class ReportDto : 
    Id:str
    Title:str
    DataType:int
    ChartsType:int
    Columns:str

    def __init__(self, model:Report) -> None:
        self.Id = model.Id.hex
        self.Title = model.Title
        self.DataType = model.DataType
        self.ChartsType = model.ChartsType
        self.Columns = model.Columns