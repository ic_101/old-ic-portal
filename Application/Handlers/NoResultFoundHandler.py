from fastapi import Request
from fastapi.responses import JSONResponse
from sqlalchemy.orm.exc import NoResultFound

class NoResultFoundHandler() : 

    async def OnException(self, request: Request, exception: NoResultFound):
        return JSONResponse (status_code = 404, content = {"message": "Элемент не найден на сервисе отчетов" })