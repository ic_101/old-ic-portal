import json
import os

class ConfigurationService : 

    _configurationData:any

    def __init__(self, ) -> None:
        self.ReadConfigurationFile()

    @property
    def OracleConnectionStringUpmo(self) : 
        return self._configurationData["connectionStringUpmo"]

    @property
    def FastApiTitle(self) : 
        return self._configurationData["title"]

    @property
    def FastApiVersion(self) : 
        return self._configurationData["version"]

    @property
    def FastApiDescription(self) : 
        return self._configurationData["description"]

    @property
    def UvicornHost(self) : 
        return self._configurationData["host"]

    @property
    def UvicornPort(self) : 
        return self._configurationData["port"]

    @property
    def JwtSecretKey(self) : 
        return self._configurationData["jwt-secret-key"]

    @property
    def JwtAlgorithm(self) : 
        return self._configurationData["jwt-algorithm"]

    def ReadConfigurationFile(self, ) : 
         with open(os.path.join(os.getcwd(), 'Application', 'Configurations', 'Web.config.json'), 'r', encoding="utf-8") as f :
            self._configurationData = json.load(f)
