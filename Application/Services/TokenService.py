from Application.Services.ConfigurationService import ConfigurationService
import jwt

class TokenService() : 

    _configurationService:ConfigurationService

    def __init__(
        self,
        configurationService:ConfigurationService) -> None:
        self._configurationService = configurationService

    def Generate(self, userDto) :

        return jwt.encode({"user": userDto }, self._configurationService.JwtSecretKey, algorithm=self._configurationService.JwtAlgorithm)

    
