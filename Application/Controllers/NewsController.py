from Startup.FastApiService import FastApiService
from Infrastructure.Services.SessionService import SessionService
from Domain.Entities.News import News

class NewsController : 

    _fastApiService:FastApiService
    _sessionService:SessionService

    def __init__(self, 
    fastApiService:FastApiService,
    sessionService:SessionService
    ) -> None:
        self._sessionService = sessionService
        self._fastApiService = fastApiService
        self._fastApiService._fastApi.add_api_route(
            "/ic-portal/api/news",
            endpoint=self.GetAll,
            methods=["GET"]
        )

    async def GetAll(self) : 

        self._sessionService.DBContext.expire_all()
        return self._sessionService.DBContext.query(News).all()