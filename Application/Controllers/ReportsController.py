from Application.Dtos.Reports.Responses.ReportDto import ReportDto
from Startup.FastApiService import FastApiService
from Infrastructure.Services.SessionService import SessionService
from Domain.Entities.Report import Report

class ReportsController : 

    _fastApiService:FastApiService
    _sessionService:SessionService

    def __init__(self, 
    fastApiService:FastApiService,
    sessionService:SessionService
    ) -> None:
        self._sessionService = sessionService
        self._fastApiService = fastApiService
        self._fastApiService._fastApi.add_api_route(
            "/ic-portal/api/reports",
            endpoint=self.GetAll,
            methods=["GET"]
        )
        self._fastApiService._fastApi.add_api_route(
            "/ic-portal/api/reports/{reportId}/result",
            endpoint=self.GetResult,
            methods=["GET"]
        )

    async def GetAll(self) : 

        self._sessionService.DBContext.expire_all()

        models = self._sessionService.DBContext.query(Report).all()
        dtos = []

        for model in models :
            dto = ReportDto(model)
            dtos.append(dto)

        return dtos

    async def GetResult(self, reportId:str) : 

        self._sessionService.DBContext.expire_all()

        model = self._sessionService.DBContext.query(Report).filter(Report.Id == reportId).one()

        result = self._sessionService.DBContext.execute(model.Query).fetchall()
        
        return result

        