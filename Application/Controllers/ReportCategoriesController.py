from Application.Dtos.ReportCategories.Responses.ReportCategoryDto import ReportCategoryDto
from Domain.Entities.ReportCategory import ReportCategory
from Startup.FastApiService import FastApiService
from Infrastructure.Services.SessionService import SessionService

class ReportCategoriesController : 

    _fastApiService:FastApiService
    _sessionService:SessionService

    def __init__(self, 
    fastApiService:FastApiService,
    sessionService:SessionService
    ) -> None:
        self._sessionService = sessionService
        self._fastApiService = fastApiService
        self._fastApiService._fastApi.add_api_route(
            "/ic-portal/api/report-categories/",
            endpoint=self.GetAll,
            methods=["GET"]
        )


    async def GetAll(self) : 

        self._sessionService.DBContext.expire_all()

        models = self._sessionService.DBContext.query(ReportCategory).all()

        dtos = []

        for model in models :
            dtos.append(ReportCategoryDto(model))

        return dtos

        


        